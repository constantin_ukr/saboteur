﻿using System.Collections.Generic;
using GameLogic.Cards.GameType;
using GameLogic.GameField;

namespace GameLogic.Cards
{
    public class CardLandslide : Card
    {
        private List<Vertex> _vertexes;

        public CardLandslide(string spriteName) : base(spriteName)
        {
            SetCardType(CardType.Landslide);

            _vertexes = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Buttom){IsActive = false},
                new Vertex(PositionType.Left){IsActive = false}
            };
        }
    }
}
