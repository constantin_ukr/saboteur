﻿using System.Collections.Generic;
using GameLogic.Cards.GameType;

namespace GameLogic.Cards
{
    public class CardUnlocker : Card
    {
        private readonly List<LockerUnlockerType> _unlockerTypes;

        public CardUnlocker(string spriteName, List<LockerUnlockerType> unlockerTypes)
            : base(spriteName)
        {
            _unlockerTypes = unlockerTypes;
            SetCardType(CardType.Unlocker);
        }

        public List<LockerUnlockerType> GetAllUnlockerTypes()
        {
            return _unlockerTypes;
        }
    }
}
