﻿using GameLogic.Cards.GameType;

namespace GameLogic.Cards
{
    public abstract class Card
    {
        public int Id { get; set; }
        protected CardType _type;
        private readonly string _spriteName;

        public Card(string spriteName)
        {
            _spriteName = spriteName;
        }

        protected void SetCardType(CardType cardType)
        {
            _type = cardType;
        }

        public CardType GeCardType()
        {
            return _type;
        }
    }
}
