﻿using GameLogic.Cards.GameType;


namespace GameLogic.Cards
{
    public class CardLocker : Card
    {
        private readonly LockerUnlockerType _locker;

        public CardLocker(string spriteName, LockerUnlockerType locker)
            : base(spriteName)
        {
            SetCardType(CardType.Locker);

            _locker = locker;
        }

        public bool IsLockByType(LockerUnlockerType lockerUnlocker)
        {
            return (_locker == lockerUnlocker);
        }
    }
}
