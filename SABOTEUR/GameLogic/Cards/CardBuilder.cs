﻿using System;
using System.Collections.Generic;
using GameLogic.Cards.GameType;
using GameLogic.GameField;

namespace GameLogic.Cards
{
    public class CardBuilder : Card
    {
        private List<Vertex> _vertexes;
        private readonly List<Edge> _edges;

        public CardBuilder(string spriteName, List<Vertex> vertexes, List<Edge> edges)
            : base(spriteName)
        {
            SetCardType(CardType.Builder);
            _vertexes = vertexes;
            _edges = edges;
        }

        public Vertex[] GetAllVertex()
        {
            return _vertexes.ToArray();
        }

        public Edge[] GetAllEdge()
        {
            return _edges.ToArray();
        }

        public void RotateCard()
        {
            // TODO: rebuild all edges

            if (_vertexes.Count > 0)
            {
                List<PositionType> visited = new List<PositionType>();

                for (int i = 0; i < _vertexes.Count; i++)
                {
                    if (!visited.Contains(_vertexes[i].PositionType))
                    {
                        Tuple<int, Vertex> oppositeVertexData = OppositeSideVertex(_vertexes[i]);

                        visited.Add(_vertexes[i].PositionType);

                        if (oppositeVertexData != null)
                        {
                            Vertex temp = new Vertex(_vertexes[i].PositionType);
                            _vertexes[i] = new Vertex(oppositeVertexData.Item2.PositionType);

                            _vertexes[oppositeVertexData.Item1] = new Vertex(temp.PositionType);
                        }
                        else
                        {
                            _vertexes[i] = new Vertex(GetOppositeSide(_vertexes[i].PositionType));
                        }

                        visited.Add(GetOppositeSide(_vertexes[i].PositionType));
                    }
                }
            }
        }

        private Tuple<int, Vertex> OppositeSideVertex(Vertex currentVertex)
        {
            PositionType oppositeType = GetOppositeSide(currentVertex.PositionType);

            for (int i = 0; i < _vertexes.Count; i++)
            {
                if (_vertexes[i].PositionType == oppositeType)
                {
                    return new Tuple<int, Vertex>(i, _vertexes[i]);
                }
            }

            return null;
        }

        private PositionType GetOppositeSide(PositionType currPositionType)
        {
            switch (currPositionType)
            {
                case PositionType.Top:
                    return PositionType.Buttom;
                case PositionType.Buttom:
                    return PositionType.Top;
                case PositionType.Left:
                    return PositionType.Right;
                case PositionType.Right:
                    return PositionType.Left;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
