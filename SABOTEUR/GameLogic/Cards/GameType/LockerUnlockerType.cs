﻿namespace GameLogic.Cards.GameType
{
    public enum LockerUnlockerType : byte
    {
        None,
        Pickaxe,
        Mine,
        Lamp
    }
}
