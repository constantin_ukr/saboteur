﻿namespace GameLogic.Cards.GameType
{
    public enum CardType : byte
    {
        Builder,
        Landslide,
        Locker,
        Unlocker,
        Start,
        Background,
        Gold,
        Coal
    }
}
