﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLogic.Cards;
using GameLogic.Cards.GameType;
using GameLogic.GameField;

namespace GameLogic.Abstract
{
    public static class Game
    {
        private static List<Player> _players = new List<Player>();
        private static bool _isGameRun = false;
        private static CardPool _pool;

        public static void StartGame()
        {
            if (_isGameRun)
            {
                return;
            }

            _players = new List<Player>(4);
            _pool = new CardPool();

            for (int i = 0; i < _players.Capacity; i++)
            {
                List<Card> cardsOnHead = new List<Card> (4);

                /*for (int j = 0; j < cardsOnHead.Capacity; j++)
                {
                    cardsOnHead.Add(_pool.GetRandomCard());
                    Thread.Sleep(10);
                }*/

                _players.Add(new Player(i, cardsOnHead));
            }

            _isGameRun = true;

        }

        public static Card TEMPGetCardById(int id)
        {
            return _pool.TEMPGetCardById(id);
        }

        public static Player TEMPGetPlayerById(int playeId)
        {
            return FindPlayerById(playeId);
        }

        public static void TEMPPrintAvailableCells()
        {
            var cells = GameFieldMatrix.GetAllAvailableCells();
            for (int i = 0; i < cells.Count; i++)
            {
                Console.WriteLine("{4}. ==>  R: {0} C: {1} cell.R: {2} cell.C: {3}",
                    cells[i].R, cells[i].C, cells[i].R / 3, cells[i].C / 3, i);
            }
        }

        public static bool PlayCardOnGameField(Card card, Position2D targetPosition)
        {
            switch (card.GeCardType())
            {
                case CardType.Builder:
                    CardBuilder cardBuilder = (CardBuilder) card;
                    if (GameFieldMatrix.PlayCardByPosition(cardBuilder.GetAllVertex(), cardBuilder.GetAllEdge(), targetPosition))
                    {
                        Console.WriteLine("Do CardBuilder");

                        return true;
                    }
                    else
                    {
                        Console.WriteLine("Undo CardBuilder");

                        return false;
                    }
                case CardType.Landslide:
                    if (GameFieldMatrix.PlayCardByByLandslide(targetPosition))
                    {
                        Console.WriteLine("Do CardLandslide");

                        return true;
                    }
                    else
                    {
                        Console.WriteLine("Undo CardLandslide");

                        return false;
                    }
                case CardType.Locker:
                    break;
                case CardType.Unlocker:
                    break;
                case CardType.Start:
                    break;
                case CardType.Background:
                    break;
                case CardType.Gold:
                    break;
                case CardType.Coal:
                    break;
            }

            return false;
        }

        public static bool PlayCardToLockOtherPlayer(CardLocker card, int otherPlayerId)
        {
            CardType cardType = card.GeCardType();

            if (cardType == CardType.Locker)
            {
                Console.WriteLine("Locker");
                return true;
            }
            else
            {
                Console.WriteLine("Not valid type");
                return false;
            }
        }

        public static bool PlayCardToUnlockOtherPlayer(CardUnlocker card, int otherPlayerId, LockerUnlockerType concreteType = LockerUnlockerType.None)
        {
            Player otherPlayer = FindPlayerById(otherPlayerId);
            List <LockerUnlockerType> allLokers = otherPlayer.GetAllLockers();

            if (allLokers.Count == 0)
            {
                Console.WriteLine("Player haven't any lockers");
                return false;
            }

            List<LockerUnlockerType> allUnlokcres = card.GetAllUnlockerTypes();

            if (concreteType != LockerUnlockerType.None)
            {
                if (allLokers.Contains(concreteType))
                {
                    Console.WriteLine("Unlocker");

                    return true;
                }
                else
                {
                    Console.WriteLine("Player haven't {0} locker", concreteType);
                    return false;
                }
            }

            for (int i = 0; i < allUnlokcres.Count; i++)
            {
                if (allLokers.Contains(allUnlokcres[i]))
                {
                    Console.WriteLine("Unlocker");

                    otherPlayer.DoUnlock(allUnlokcres[i]);

                    return true;
                }
            }

            return false;
        }

        public static void SetCardsPlayerToHand(int playerId, Card[] cards)
        {
            Player p = FindPlayerById(playerId);
            p.SetCardsOnHand(cards);
        }

        private static Player FindPlayerById(int playerId)
        {
            return _players.First(p => p.Id == playerId);
        }
    }
}
