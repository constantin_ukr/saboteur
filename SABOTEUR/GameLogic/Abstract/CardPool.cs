﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLogic.Cards;
using GameLogic.GameField;

namespace GameLogic.Abstract
{
    internal class CardPool
    {
        List<Card> _cards = new List<Card>();

        public CardPool()
        {
            CreateBlockerCrads();
            CreateBuilderCrads();
            CreateLandslideCrads();
            //AddIdToCard();
        }

        public Card GetRandomCard()
        {
            int index = new Random().Next(0, _cards.Count - 1);

            Card card = _cards[index];
            _cards.RemoveAt(index);

            return card;
        }

        public Card TEMPGetCardById(int id)
        {
            return _cards.First(c => c.Id == id);
        }

        private void CreateBlockerCrads()
        {
            List<Vertex> vertexesBR = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Buttom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = false}
            };

            var blockerBR = new CardBuilder("", vertexesBR, new List<Edge>(0));
            blockerBR.Id = 0;
            _cards.Add(blockerBR);

            List<Vertex> vertexesBL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Buttom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true}
            };

            var blockerBL = new CardBuilder("", vertexesBL, new List<Edge>(0));
            blockerBL.Id = 1;
            _cards.Add(blockerBL);

            List<Vertex> vertexesTB = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Buttom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = false}
            };

            var blockerTB = new CardBuilder("", vertexesTB, new List<Edge>(0));
            blockerTB.Id = 2;
            _cards.Add(blockerTB);

            List<Vertex> vertexesR = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Buttom){IsActive = false},
                new Vertex(PositionType.Left){IsActive = false}
            };

            var blockerR = new CardBuilder("", vertexesR, new List<Edge>(0));
            blockerR.Id = 3;
            _cards.Add(blockerR);

            List<Vertex> vertexesTBR = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Buttom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = false}
            };

            var blockerTBR = new CardBuilder("", vertexesTBR, new List<Edge>(0));
            blockerTBR.Id = 4;
            _cards.Add(blockerTBR);

            List<Vertex> vertexesTBRL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Buttom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true}
            };

            var blockerTBRL = new CardBuilder("", vertexesTBRL, new List<Edge>(0));
            blockerTBRL.Id = 5;
            _cards.Add(blockerTBRL);

            List<Vertex> vertexesRL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Buttom){IsActive = false},
                new Vertex(PositionType.Left){IsActive = true}
            };

            var blockerRL = new CardBuilder("", vertexesRL, new List<Edge>(0));
            blockerRL.Id = 6;
            _cards.Add(blockerRL);
            _cards.Add(blockerRL);

            List<Vertex> vertexesT = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Buttom){IsActive = false},
                new Vertex(PositionType.Left){IsActive = false}
            };

            var blockerT = new CardBuilder("", vertexesT, new List<Edge>(0));
            blockerT.Id = 7;
            _cards.Add(blockerT);


            List<Vertex> vertexesTRL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Buttom){IsActive = false},
                new Vertex(PositionType.Left){IsActive = true}
            };

            var blockerTRL = new CardBuilder("", vertexesTRL, new List<Edge>(0));
            blockerTRL.Id = 8;
            _cards.Add(blockerTRL);
        }

        private void CreateBuilderCrads()
        {
            List<Vertex> vertexesRL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Buttom){IsActive = false},
                new Vertex(PositionType.Left){IsActive = true}
            };

            List<Edge> edgesRL = new List<Edge>()
            {
                new Edge(vertexesRL[1], vertexesRL[3])
            };

            var builderRL = new CardBuilder("", vertexesRL, edgesRL);
            builderRL.Id = 10;
            _cards.Add(builderRL);
            _cards.Add(builderRL);
            _cards.Add(builderRL);
            _cards.Add(builderRL);
            _cards.Add(builderRL);

            //------------------------------------------

            List<Vertex> vertexesBRL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Buttom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true}
            };

            List<Edge> edgesBRL = new List<Edge>()
            {
                new Edge(vertexesBRL[2], vertexesBRL[1]),
                new Edge(vertexesBRL[1], vertexesBRL[3]),
                new Edge(vertexesBRL[3], vertexesBRL[2])
            };

            var builderBRL = new CardBuilder("", vertexesBRL, edgesBRL);
            builderBRL.Id = 11;
            _cards.Add(builderBRL);
            _cards.Add(builderBRL);
            _cards.Add(builderBRL);
            _cards.Add(builderBRL);
            _cards.Add(builderBRL);

            //------------------------------------------

            List<Vertex> vertexesBR = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Buttom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = false}
            };

            List<Edge> edgesBR = new List<Edge>()
            {
                new Edge(vertexesBR[2], vertexesBR[1])
            };

            var builderBR = new CardBuilder("", vertexesBR, edgesBR);
            builderBR.Id = 12;
            _cards.Add(builderBR);
            _cards.Add(builderBR);
            _cards.Add(builderBR);
            _cards.Add(builderBR);
            _cards.Add(builderBR);

            //------------------------------------------

            List<Vertex> vertexesBL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = false},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Buttom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true}
            };

            List<Edge> edgesBL = new List<Edge>()
            {
                new Edge(vertexesBL[2], vertexesBL[3])
            };

            var builderBL = new CardBuilder("", vertexesBL, edgesBL);
            builderBL.Id = 13;
            _cards.Add(builderBL);
            _cards.Add(builderBL);
            _cards.Add(builderBL);
            _cards.Add(builderBL);
            _cards.Add(builderBL);

            //------------------------------------------

            List<Vertex> vertexesTB = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Buttom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = false}
            };

            List<Edge> edgesTB = new List<Edge>()
            {
                new Edge(vertexesTB[0], vertexesTB[2])
            };

            var builderTB = new CardBuilder("", vertexesTB, edgesTB);
            builderTB.Id = 14;
            _cards.Add(builderTB);
            _cards.Add(builderTB);
            _cards.Add(builderTB);
            _cards.Add(builderTB);
            _cards.Add(builderTB);

            //------------------------------------------

            List<Vertex> vertexesTBL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = false},
                new Vertex(PositionType.Buttom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true}
            };

            List<Edge> edgesTBL = new List<Edge>()
            {
                new Edge(vertexesTB[0], vertexesTB[2]),
                new Edge(vertexesTB[2], vertexesTB[3]),
                new Edge(vertexesTB[3], vertexesTB[0])
            };

            var builderTBL = new CardBuilder("", vertexesTBL, edgesTBL);
            builderTBL.Id = 15;
            _cards.Add(builderTBL);
            _cards.Add(builderTBL);
            _cards.Add(builderTBL);
            _cards.Add(builderTBL);
            _cards.Add(builderTBL);

            //------------------------------------------

            List<Vertex> vertexesTBRL = new List<Vertex>
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Right){IsActive = true},
                new Vertex(PositionType.Buttom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true}
            };

            List<Edge> edgesTBRL = new List<Edge>()
            {
                new Edge(vertexesTB[0], vertexesTB[1]),
                new Edge(vertexesTB[1], vertexesTB[2]),
                new Edge(vertexesTB[2], vertexesTB[3]),
                new Edge(vertexesTB[3], vertexesTB[0])
            };

            var builderTBRL = new CardBuilder("", vertexesTBRL, edgesTBRL);
            builderTBRL.Id = 16;
            _cards.Add(builderTBRL);
            _cards.Add(builderTBRL);
            _cards.Add(builderTBRL);
            _cards.Add(builderTBRL);
            _cards.Add(builderTBRL);
        }

        private void CreateLandslideCrads()
        {
            _cards.Add(new CardLandslide(string.Empty){Id = 20});
        }

        private void AddIdToCard()
        {
            for (int i = 0; i < _cards.Count; i++)
            {
                _cards[i].Id = i;
            }
        }

    }
}
