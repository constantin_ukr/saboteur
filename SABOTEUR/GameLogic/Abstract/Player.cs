﻿using System;
using System.Collections.Generic;
using GameLogic.Cards;
using GameLogic.Cards.GameType;
using GameLogic.GameField;

namespace GameLogic.Abstract
{
    public class Player
    {
        public int Id { get; }
        private List<Card> _cardsOnHand;
        private List<LockerUnlockerType> _lockers;

        public Player(int id, List<Card> startCardsOnHand)
        {
            Id = id;
            _cardsOnHand = startCardsOnHand;
            _lockers = new List<LockerUnlockerType>(0);
        }

        public void DoLock(LockerUnlockerType type)
        {
            _lockers.Add(type);
        }

        public bool DoUnlock(LockerUnlockerType type)
        {
            if (IsLockByType(type))
            {
                _lockers.Remove(type);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void PlayCardOnField(Card card, Position2D targetPosition)
        {
            if (Game.PlayCardOnGameField(card, targetPosition))
            {
                RemoveCardsFromPlayerHand(new Card[] { card });
            }
            else
            {
                InvalidPlay();
            }

        }

        public void PlayCardToLockOtherPlayerById(Card card, int otherPlayerId)
        {
            if (!(card is CardLocker lockerCard) || !Game.PlayCardToLockOtherPlayer(lockerCard, otherPlayerId))
            {
                InvalidPlay();
            }
        }

        public void SetCardsOnHand(Card[] cards)
        {
            _cardsOnHand.AddRange(cards);
        }

        public List<Card> GetHandGards()
        {
            return _cardsOnHand;
        }

        public List<LockerUnlockerType> GetAllLockers()
        {
            return _lockers;
        }

        private void InvalidPlay()
        {
            Console.WriteLine("You cann't play this card");
        }

        private void RemoveCardsFromPlayerHand(Card[] dropedCards)
        {
            for (int i = 0; i < dropedCards.Length; i++)
            {
                _cardsOnHand.Remove(dropedCards[i]);
            }
        }

        private bool IsLockByType(LockerUnlockerType type)
        {
            for (int i = 0; i < _lockers.Count; i++)
            {
                if (_lockers[i] == type)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
