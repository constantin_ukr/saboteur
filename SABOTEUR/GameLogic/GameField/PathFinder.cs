﻿using System;

namespace GameLogic.GameField
{
    internal class PathFinder
    {
        public void FindWave(int startX, int startY, int targetX, int targetY, int mapHeight, int mapWidht,
            int[,] matrixMap)
        {
            int[,] cMap = CreateMap(mapHeight, mapWidht, matrixMap);

            int step = 0;
            cMap[targetX, targetY] = 0; //Начинаем с финиша
            while (true)
            {
                for (int row = 0; row < mapHeight; row++)
                {
                    for (int col = 0; col < mapWidht; col++)
                    {
                        if (cMap[row, col] == step)
                        {
                            //Ставим значение шага+1 в соседние ячейки (если они проходимы)
                            if (!IsGoBeyondGameField(col - 1, row - 1, mapWidht, mapHeight)
                                && cMap[row - 1, col] != -2
                                && cMap[row - 1, col] == -1)
                            {
                                cMap[row - 1, col] = step + 1;
                            }
                            if (!IsGoBeyondGameField(col - 1, row - 1, mapWidht, mapHeight)
                                && cMap[row, col - 1] != -2
                                && cMap[row, col - 1] == -1)
                            {
                                cMap[row, col - 1] = step + 1;
                            }
                            if (!IsGoBeyondGameField(col + 1, row + 1, mapWidht, mapHeight)
                                && cMap[row + 1, col] != -2
                                && cMap[row + 1, col] == -1)
                            {
                                cMap[row + 1, col] = step + 1;
                            }
                            if (!IsGoBeyondGameField(col + 1, row + 1, mapWidht, mapHeight)
                                && cMap[row, col + 1] != -2
                                && cMap[row, col + 1] == -1)
                            {
                                cMap[row, col + 1] = step + 1;
                            }
                        }
                    }
                }

                step++;

                if (cMap[startX, startY] != -1)
                {
                    Console.WriteLine("can");
                    Console.WriteLine(step);
                    break;
                }
                if (step > matrixMap.Length)
                {
                    Console.WriteLine("can not");
                    break;
                }
            }

            PrintMap(mapHeight, mapWidht, startX, startY, targetX, targetY, cMap);
        }

        private int[,] CreateMap(int mapHeight, int mapWidht, int[,] map)
        {
            int[,] matrixMap = new int[mapHeight, mapWidht];

            for (int row = 0; row < mapHeight; row++)
            {
                for (int col = 0; col < mapWidht; col++)
                {
                    // TODO: SB-10. Add indicator cross path 
                    if (map[row, col] == 0)
                    {
                        matrixMap[row, col] = -2; //индикатор стены
                    }
                    else
                    {
                        matrixMap[row, col] = -1; //индикатор еще не ступали сюда
                    }
                }
            }

            return matrixMap;
        }

        private void PrintMap(int mapHeight, int mapWidht, int startX, int startY, int targetX, int targetY, int[,] cMap)
        {
            for (int row = 0; row < mapHeight; row++)
            {
                Console.WriteLine();
                for (int col = 0; col < mapWidht; col++)
                {
                    if (row == startY && col == startX)
                    {
                        Console.Write("SS");
                    }
                    else if (cMap[row, col] == -1)
                    {
                        Console.Write("  ");
                    }
                    else if (cMap[row, col] == -2)
                    {
                        Console.Write("##");
                    }
                    else if (row == targetY && col == targetX)
                    {
                        Console.Write("FF");
                    }
                    else if (cMap[row, col] > -1)
                    {
                        Console.Write("{0:0#}", cMap[row, col]);
                    }
                }
            }

            Console.WriteLine();
        }

        private static bool IsGoBeyondGameField(int x, int y, int width, int height)
        {
            return ((x == -1 || x == width) || (y == -1 || y == height));
        }
    }
}
