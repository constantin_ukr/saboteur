﻿using System;
using System.Collections.Generic;

namespace GameLogic.GameField
{
    public class GraphData
    {
        private const byte InvalidData = 0;
        private const byte EmptyAnchor = 1;
        private const byte Builder = 2;
        private const byte Crossroad = 3;
        private const byte JoinOppositeSides = 4;
        private const byte Deadlock = 9;
        private const byte StartGame = 13;
        private const byte Finish = 15;

        private const int CardSizeArraySize = 3;

        private int _width;
        private int _height;

        public byte[,] _matrixPath;

        public GraphData(int gameFieldHeight, int gameFieldWidth)
        {
            _width = gameFieldWidth * CardSizeArraySize;
            _height = gameFieldHeight * CardSizeArraySize;

            _matrixPath = new byte[_height, _width];

            int r = 0;
            int c = 0;

            for (int row = 0; row < _height; row++)
            {
                for (int col = 0; col < _width; col++)
                {
                    if (r == 1)
                    {
                        _matrixPath[row, col] = (c != 1) ? EmptyAnchor : InvalidData;
                    }
                    else
                    {
                        _matrixPath[row, col] = (c != 1) ? InvalidData : EmptyAnchor;
                    }
                    c = GetCardIteratorSize(c);
                }
                r = GetCardIteratorSize(r);
            }
        }

        public void SetData(byte[,] cardArray, Position2D targetPosition)
        {
            for (int row = targetPosition.R * CardSizeArraySize, i = 0; i < CardSizeArraySize; row++, i++)
            {
                for (int col = targetPosition.C * CardSizeArraySize, j = 0; j < CardSizeArraySize; col++, j++)
                {
                    _matrixPath[row, col] = cardArray[i, j];
                }
            }
        }

        public List<Position2D> GetAllAvailablePoints()
        {
            return GetAllAvailablePoints(_matrixPath);
        }

        public void PrintMatrix()
        {
            for (int row = 0; row < _height; row++)
            {
                for (int col = 0; col < _width; col++)
                {
                    string marker = string.Empty;

                    if (_matrixPath[row, col] == 0)
                    {
                        marker = " ";
                    }
                    else if (_matrixPath[row, col] == 1)
                    {
                        marker = "+";
                    }
                    else
                    {
                        marker = "*";
                    }

                    Console.Write(marker);
                }
                Console.WriteLine();
            }
        }

        public List<Position2D> GetAllAvailablePoints(byte[,] matrix)
        {
            List<Position2D> points = new List<Position2D>();

            for (int row = 0; row < _height; row++)
            {
                for (int col = 0; col < _width; col++)
                {
                    if (matrix[row, col] == 0)
                    {
                        continue;
                    }

                    int value = matrix[row, col];

                    if (value == 2)
                    {
                        points.AddRange(GetValidPositionValue(row, col));
                    }
                }
            }

            return points;
        }

        private bool IsPositionOutsideMatrix(int width, int height, Position2D targetPosition)
        {
            return targetPosition.R == -1 || targetPosition.C == -1 || targetPosition.C >= width || targetPosition.R >= height;
        }

        // TODO: MUST BE FIX
        private List<Position2D> GetValidPositionValue(int currX, int currY)
        {
            List<Position2D> values = new List<Position2D>();

            List<Position2D> possibleCoords = new List<Position2D>
            {
                new Position2D(currX - 1, currY),
                new Position2D(currX + 1, currY),
                new Position2D(currX, currY - 1),
                new Position2D(currX, currY + 1)
            };

            for (int i = 0; i < possibleCoords.Count; i++)
            {
                Position2D value = GetValue(possibleCoords[i]);
                if (value != null)
                {
                    values.Add(value);
                }
            }

            return values;
        }

        // TODO: MUST BE FIX Verify minus value
        private Position2D GetValue(Position2D targetPosition)
        {
            if (!IsPositionOutsideMatrix(_width, _height, targetPosition))
            {
                if (_matrixPath[targetPosition.R, targetPosition.C] == 1)
                {
                    return targetPosition;
                }
            }

            return null;
        }

        private int GetCardIteratorSize(int size)
        {
            if (size == CardSizeArraySize)
            {
                size = 0;
            }

            return ++size;
        }
    }
}
