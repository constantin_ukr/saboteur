﻿using System;

namespace GameLogic.GameField
{
    [Flags]
    public enum PositionType : byte
    {
        None = 0,
        Top = 1,
        Buttom = 2,
        Left = 4,
        Right = 8
    }
}
