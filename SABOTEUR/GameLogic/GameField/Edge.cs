﻿namespace GameLogic.GameField
{
    public class Edge
    {
        public int Id { get; set; }
        public Vertex VertexOneId { get; }
        public Vertex VertexTwoId { get; }

        public Edge(Vertex vertexOne, Vertex vertexTwo)
        {
            VertexOneId = vertexOne;
            VertexTwoId = vertexTwo;
        }
    }
}
