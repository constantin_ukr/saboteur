﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace GameLogic.GameField
{
    public static class GameFieldMatrix
    {
        private static int _width;
        private static int _height;

        private static Cell[,] _gameField;
        private static GraphData _data;

        private static List<Position2D> _startPositions;
        private static List<Position2D> _finishPositions;

        private static IReadOnlyList<Position2D> _availablePosition;

        public static void CreateMatrix(int height, int width, Position2D startPosition)
        {
            _width = width;
            _height = height;

            _gameField = new Cell[height, width];

            _startPositions = new List<Position2D>(1);
            _finishPositions = new List<Position2D>(3);

            for (int row = 0; row < _height; row++)
            {
                for (int col = 0; col < _width; col++)
                {
                    _gameField[row, col] = new Cell {Position = new Position2D(row, col)};
                }
            }

            _data = new GraphData(height, width);

            Vertex[] vertexes = new Vertex[]
            {
                new Vertex(PositionType.Top){IsActive = true},
                new Vertex(PositionType.Buttom){IsActive = true},
                new Vertex(PositionType.Left){IsActive = true},
                new Vertex(PositionType.Right){IsActive = true}
            };

            byte mask = CreateMaskFromVertexes(vertexes);

            Edge[] edges = new Edge[]
            {
                new Edge(vertexes[0], vertexes[1]),
                new Edge(vertexes[1], vertexes[2]),
                new Edge(vertexes[2], vertexes[3]),
                new Edge(vertexes[3], vertexes[0]),
            };

            _gameField[startPosition.R, startPosition.C] = new Cell()
            {
                IsEmpty = false,
                Vertexes = vertexes,
                Edges = edges,
                Mask = mask,
                Position = startPosition
            };

            byte[,] card = CreateCardArrayFromCell(_gameField[startPosition.R, startPosition.C]);
            _data.SetData(card, startPosition);
            _startPositions.Add(new Position2D(startPosition.R, startPosition.C));

            Position2D finishPosition = new Position2D(startPosition.R, startPosition.C + 8);
            _gameField[finishPosition.R, finishPosition.C] = new Cell()
            {
                IsEmpty = false,
                Vertexes = vertexes,
                Edges = edges,
                Mask = mask,
                Position = finishPosition
            };
            card = CreateCardArrayFromCell(_gameField[finishPosition.R, finishPosition.C]);
            _data.SetData(card, finishPosition);
            _finishPositions.Add(finishPosition);

            finishPosition = new Position2D(startPosition.R - 2, startPosition.C + 8);
            _gameField[finishPosition.R, finishPosition.C] = new Cell()
            {
                IsEmpty = false,
                Vertexes = vertexes,
                Edges = edges,
                Mask = mask,
                Position = finishPosition
            };
            card = CreateCardArrayFromCell(_gameField[finishPosition.R, finishPosition.C]);
            _data.SetData(card, finishPosition);
            _finishPositions.Add(finishPosition);

            finishPosition = new Position2D(startPosition.R + 2, startPosition.C + 8);
            _gameField[finishPosition.R, finishPosition.C] = new Cell()
            {
                IsEmpty = false,
                Vertexes = vertexes,
                Edges = edges,
                Mask = mask,
                Position = finishPosition
            };
            card = CreateCardArrayFromCell(_gameField[finishPosition.R, finishPosition.C]);
            _data.SetData(card, finishPosition);
            _finishPositions.Add(finishPosition);

            _data.PrintMatrix();

            CalculateAllAvailableCells();
        }

        public static Cell GetStatusCellByCoords(Position2D targetPosition)
        {
            if (Validate(targetPosition))
            {
                return _gameField[targetPosition.R, targetPosition.C];
            }
            else
            {
                throw new Exception("Fatal error");
            }
        }

        public static bool PlayCardByPosition(Vertex[] vertexes, Edge[] edges, Position2D targetPosition)
        {
            if (Validate(targetPosition)
                && GetStatusCellByCoords(targetPosition).IsEmpty
                && ValidateCellAround(vertexes, targetPosition))
            {
                UpdateCellInPosition(vertexes, edges, targetPosition);

                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool PlayCardByByLandslide(Position2D targetPosition)
        {
            if (Validate(targetPosition)
                && !GetStatusCellByCoords(targetPosition).IsEmpty
                && !DoesPositionIsImmutable(targetPosition))
            {
                Cell cell = new Cell
                {
                    IsEmpty = true,
                    Vertexes = new Vertex[]{
                        new Vertex(PositionType.Top),
                        new Vertex(PositionType.Buttom),
                        new Vertex(PositionType.Left),
                        new Vertex(PositionType.Right)
                    },
                    Edges = new Edge[0],
                    Position = new Position2D(targetPosition.R, targetPosition.C),
                    Mask = 0
                };

                _gameField[targetPosition.R, targetPosition.C] = cell;

                UpdateCellInPosition(cell.Vertexes, cell.Edges, cell.Position, true);
                return true;
            }
            else
            {
                return false;
            }
        }

        public static IReadOnlyList<Position2D> GetAllAvailableCells()
        {
            return _availablePosition;
        }

        private static void CalculateAllAvailableCells(bool mustCalculate = false)
        {
            var tmp = _data.GetAllAvailablePoints();

            List<Position2D> notAvailablePositionsAroundFinishCell = new List<Position2D>();

            for (int j = 0; j < _finishPositions.Count; j++)
            {
                Cell fcell = _gameField[_finishPositions[j].R, _finishPositions[j].C];
                if (!ValidateCellAround(fcell.Vertexes, fcell.Position))
                {
                    notAvailablePositionsAroundFinishCell.Add(new Position2D(fcell.Position.R * 3 + 3, fcell.Position.C * 3 + 1));
                    notAvailablePositionsAroundFinishCell.Add(new Position2D(fcell.Position.R * 3 - 1, fcell.Position.C * 3 + 1));
                    notAvailablePositionsAroundFinishCell.Add(new Position2D(fcell.Position.R * 3 + 1, fcell.Position.C * 3 + 3));
                    notAvailablePositionsAroundFinishCell.Add(new Position2D(fcell.Position.R * 3 + 1, fcell.Position.C * 3 - 1));
                }
            }

            for (int i = 0; i < tmp.Count; i++)
            {
                bool desreese = false;

                for (int j = 0; j < notAvailablePositionsAroundFinishCell.Count; j++)
                {
                    if (notAvailablePositionsAroundFinishCell[j].R == tmp[i].R && notAvailablePositionsAroundFinishCell[j].C == tmp[i].C)
                    {
                        tmp.Remove(tmp[i]);
                        desreese = true;
                        break;
                    }
                }

                if (desreese)
                {
                    --i;
                }
            }

            if (mustCalculate)
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                
                
                for (int i = 0; i < tmp.Count; i++)
                {
                    Console.WriteLine();
                    int[,] array = new int[30, 30];
                    Array.Copy(_data._matrixPath, array, _data._matrixPath.Length);
                    PathFinder path = new PathFinder();

                    Position2D start = new Position2D(_startPositions[0].R * 3 + 1, _startPositions[0].C * 3 + 1);

                    path.FindWave(start.R, start.C, tmp[i].R, tmp[i].C, 30, 30, array);
                }
                stopWatch.Stop();

                Console.WriteLine(stopWatch.Elapsed.Milliseconds);
            }

            _availablePosition = tmp;
        }

        private static bool DoesPositionIsImmutable(Position2D targetPosition)
        {
            return IsListContainsPosition(_startPositions, targetPosition) || IsListContainsPosition(_finishPositions, targetPosition);
        }

        private static bool IsListContainsPosition(List<Position2D> source, Position2D targetPosition)
        {
            for (int i = 0; i < source.Count; i++)
            {
                if (source[i].R == targetPosition.R && source[i].C == targetPosition.C)
                {
                    return true;
                }
            }

            return false;
        }

        private static void UpdateCellInPosition(Vertex[] vertices, Edge[] edges, Position2D targetPosition, bool mustCalculate = false)
        {
            Cell cell = _gameField[targetPosition.R, targetPosition.C];
            cell.IsEmpty = false;
            cell.Edges = edges;
            cell.Vertexes = vertices;
            cell.Mask = CreateMaskFromVertexes(vertices);
            cell.Position = targetPosition;

            _data.SetData(CreateCardArrayFromCell(cell), targetPosition);
            Console.WriteLine();
            _data.PrintMatrix();

            if (mustCalculate)
            {
                CalculateAllAvailableCells(true);
            }
        }

        private static byte[,] CreateCardArrayFromCell(Cell cell)
        {
            Vertex[] activeVertexs = cell.Vertexes.Where(v => v.IsActive).ToArray();
            
            bool isTop = BitMaskHelper.HasFlagInMask(cell.Mask, Convert.ToByte(PositionType.Top));
            bool isButtom = BitMaskHelper.HasFlagInMask(cell.Mask, Convert.ToByte(PositionType.Buttom));
            bool isLeft = BitMaskHelper.HasFlagInMask(cell.Mask, Convert.ToByte(PositionType.Left));
            bool isRight = BitMaskHelper.HasFlagInMask(cell.Mask, Convert.ToByte(PositionType.Right));

            byte[,] cardArray = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

            Vertex[] connected = GetConnectVectors(activeVertexs, cell.Edges);
            
            byte maskConnect = CreateMaskFromVertexes(connected);

            if (connected.Length == 1)
            {
                throw new Exception("not correct value"); 
            }

            if(connected.Length == 4)
            {
                cardArray[1, 1] = 3;
            }
            else if(connected.Length == 4 && cell.Edges.Length == 2)
            {
                cardArray[1, 1] = 4;
            }
            else if (connected.Length > 0)
            {
                cardArray[1, 1] = 2;
            }
            else if (connected.Length == 0) //landslide card
            {
                cardArray = new byte[,]{ { 0, 1, 0 }, { 1, 0, 1 }, { 0, 1, 0 } };
            }

            if (isTop)
            {
                cardArray[0, 1] = Convert.ToByte(BitMaskHelper.HasFlagInMask(maskConnect, Convert.ToByte(PositionType.Top)) ? 2 : 9);
            }
            if (isButtom)
            {
                cardArray[2, 1] = Convert.ToByte(BitMaskHelper.HasFlagInMask(maskConnect, Convert.ToByte(PositionType.Buttom)) ? 2 : 9);
            }
            if (isLeft)
            {
                cardArray[1, 0] = Convert.ToByte(BitMaskHelper.HasFlagInMask(maskConnect, Convert.ToByte(PositionType.Left)) ? 2 : 9);
            }
            if (isRight)
            {
                cardArray[1, 2] = Convert.ToByte(BitMaskHelper.HasFlagInMask(maskConnect, Convert.ToByte(PositionType.Right)) ? 2 : 9);
            }

            return cardArray;
        }

        private static Vertex[] GetConnectVectors(Vertex[] vertices, Edge[] edges)
        {
            HashSet<Vertex> connectedVertices = new HashSet<Vertex>();

            if (edges == null || edges.Length == 0)
            {
                return new Vertex[0];
            }

            for (int i = 0; i < edges.Length; i++)
            {
                for (int j = 0; j < vertices.Length; j++)
                {
                    if (edges[i].VertexOneId.PositionType == vertices[i].PositionType || edges[i].VertexTwoId.PositionType == vertices[i].PositionType)
                    {
                        connectedVertices.Add(vertices[j]);
                    }
                }
            }

            return connectedVertices.ToArray();
        }

        private static bool Validate(Position2D targetPosition)
        {
            return (targetPosition.C >= 0 && targetPosition.C < _width) && (targetPosition.R >= 0 && targetPosition.R < _height);
        }

        private static bool ValidateCellAround(Vertex[] checkingCardVertexes, Position2D targetPosition) // active ancher card play 
        {
            bool[] result = new bool[4];
            byte activePositionPlayingCardMask = CreateMaskFromVertexes(checkingCardVertexes);

            int possibleActiveConnections = 0;

            result[0] = Validate(new Position2D(targetPosition.R - 1, targetPosition.C))
                && ValidateCell(
                    _gameField[targetPosition.R - 1, targetPosition.C],
                    ref possibleActiveConnections,
                    activePositionPlayingCardMask,
                    PositionType.Top);

            result[1] = Validate(new Position2D(targetPosition.R + 1, targetPosition.C))
                && ValidateCell(
                    _gameField[targetPosition.R + 1, targetPosition.C],
                    ref possibleActiveConnections,
                    activePositionPlayingCardMask,
                    PositionType.Buttom);

            result[2] = Validate(new Position2D(targetPosition.R, targetPosition.C - 1))
                && ValidateCell(
                    _gameField[targetPosition.R, targetPosition.C - 1],
                    ref possibleActiveConnections,
                    activePositionPlayingCardMask,
                    PositionType.Left);

            result[3] = Validate(new Position2D(targetPosition.R, targetPosition.C + 1))
                && ValidateCell(
                    _gameField[targetPosition.R, targetPosition.C + 1],
                    ref possibleActiveConnections,
                    activePositionPlayingCardMask,
                    PositionType.Right);

            if (possibleActiveConnections == 0)
            {
                return false;
            }

            return result.Any(i => i);
        }

        private static bool ValidateCell(Cell otherCell, ref int connectionCount, byte activePositionPlayingCardMask, PositionType position = PositionType.None)
        {
            bool isIncrease = CanConnectToPossition(otherCell, activePositionPlayingCardMask, position);

            if (isIncrease)
            {
                ++connectionCount;
            }

            return (isIncrease || otherCell.IsEmpty);
        }

        private static bool CanConnectToPossition(Cell otherCell, byte activePositionPlayingCardMask, PositionType position)
        {
            bool result = false;

            if (!otherCell.IsEmpty)
            {
                for (int i = 0; i < otherCell.Vertexes.Length; i++)
                {
                    if (otherCell.Vertexes[i].IsActive && BitMaskHelper.HasFlagInMask(activePositionPlayingCardMask, Convert.ToByte(position)))
                    {
                        result = true;
                        break;
                    }
                }
            }

            return result;
        }

        private static byte CreateMaskFromVertexes(Vertex[] vertexes)
        {
            byte mask = 0;

            for (int i = 0; i < vertexes.Length; i++)
            {
                if (vertexes[i].IsActive)
                {
                    byte flag = Convert.ToByte(vertexes[i].PositionType);
                    BitMaskHelper.AddFlagToMask(ref mask, flag);
                }
            }

            return mask;
        }
    }
}
