﻿namespace GameLogic.GameField
{
    public class Vertex
    {
        public bool IsActive { get; set; }

        public PositionType PositionType { get; set; }

        public Vertex(PositionType positionType)
        {
            PositionType = positionType;
            IsActive = false;
        }
    }
}
