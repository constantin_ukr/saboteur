﻿namespace GameLogic.GameField
{
    public class Cell
    {
        public bool IsEmpty { get; set; }
        public Position2D Position { get; set; }

        public Vertex[] Vertexes { get; set; }
        public Edge[] Edges { get; set; }

        public byte Mask { get; set; }

        public Cell()
        {
            IsEmpty = true;
            Position = new Position2D();
            Vertexes = new Vertex[]
            {
                new Vertex(PositionType.Top),
                new Vertex(PositionType.Buttom),
                new Vertex(PositionType.Left),
                new Vertex(PositionType.Right)
            };
        }
    }
}
