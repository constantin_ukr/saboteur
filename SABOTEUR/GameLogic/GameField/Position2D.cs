﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameLogic.GameField
{
    public class Position2D
    {
        public int R { get; set; }
        public int C { get; set; }

        public Position2D()
        {
            R = 0;
            C = 0;
        }

        public Position2D(int row, int col)
        {
            R = row;
            C = col;
        }
    }
}
