﻿using GameLogic.GameField;
using System;
using System.Collections.Generic;
using GameLogic.Abstract;
using GameLogic.Cards;

namespace SABOTEUR
{
    class Program
    {
        static void Main(string[] args)
        {
            InitGameField();

            Console.ReadKey();
        }

        private static void InitGameField()
        {
            GameFieldMatrix.CreateMatrix(10, 10, new Position2D(5, 1));

            Game.StartGame();
            
            Player one = Game.TEMPGetPlayerById(0);
            Card[] cards = new Card[]
            {
                Game.TEMPGetCardById(10),
                Game.TEMPGetCardById(11),
                Game.TEMPGetCardById(12),
                Game.TEMPGetCardById(16),
                Game.TEMPGetCardById(20)
            };
            one.SetCardsOnHand(cards);


            Player two = Game.TEMPGetPlayerById(1);
            Player three = Game.TEMPGetPlayerById(2);
            Player four = Game.TEMPGetPlayerById(3);
            Game.TEMPPrintAvailableCells();

            one.PlayCardOnField((CardBuilder)cards[0], new Position2D(5, 2));
            one.PlayCardOnField((CardBuilder)cards[1], new Position2D(5, 3));
            one.PlayCardOnField((CardBuilder)cards[3], new Position2D(5, 4));
            one.PlayCardOnField((CardBuilder)cards[3], new Position2D(5, 0));
            one.PlayCardOnField((CardLandslide)cards[4], new Position2D(5, 1));
            one.PlayCardOnField((CardLandslide)cards[4], new Position2D(5, 0));

            Game.TEMPPrintAvailableCells();
            Console.ReadKey();
        }
    }
}
